angular
  .module("questions.service", [])
  .service("QuestionsService", function ($http) {
    this.url = "http://localhost:3000/questions";
    /* QUESTIONS */
    this.fetchQuestions = (id) => {
      return $http
        .get(this.url + "?id_quiz=" + id)

        .then((res) => {
          return (this._questions = res.data);
        });
    };
    this.updateQuestion = (draft) => {
      return $http.put(this.url + "/" + draft.id, draft);
    };
    this.addNewQuestion = (draft) => {
      return $http.post(this.url + "/", draft);
    };
    this.deleteQuestionById = (id) => {
      return $http.delete(this.url + `/${id}`).then((resp) => resp.data);
    };
  });
