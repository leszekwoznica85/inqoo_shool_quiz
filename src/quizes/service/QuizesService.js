angular.module("quizes.service", []).service("QuizesService", function ($http) {
  this.url = "http://localhost:3000/quiz/";
  this.update = (draft) => {
    return $http.put(this.url + draft.id, draft);
  };
  this.getQuizById = (id) => {
    return this._quizes.find((u) => u.id == id);
  };
  this.fetchQuizes = () => {
    return $http.get(this.url).then((res) => {
      return (this._quizes = res.data);
    });
  };

  this.addNew = (draft) => {
    return $http.post(this.url, draft);
  };
  this.deleteQuizById = (id) => {
    return $http.delete(this.url + `/${id}`).then((resp) => resp.data);
  };
});
