angular
  .module("answer.services", [])
  .service("AnswerService", function ($http) {
    this.url = "http://localhost:3000/answers";

    this.fetchAnswers = (id) => {
      return $http.get(this.url + "?id_question=" + id).then((res) => {
        return (this._answers = res.data);
      });
    };
    this.updateAnswer = (draft) => {
      return $http.put(this.url + "/" + draft.id, draft);
    };
    this.addNewAnswer = (draft) => {
      console.log(draft)
      return $http.post(this.url + "/", draft);
    };
    this.deleteAnswerById = (id) => {
      return $http.delete(this.url + `/${id}`).then((resp) => resp.data);
    };
  });
