angular.module("quizes").component("quizAnswer", {
  bindings: {
    selectedQuestion: "<",
    modeQuestion: "=",
    onSelect: "&",
    modeQuizList: "=",
    modeAnswer: "=",
  },
  templateUrl: "../src/quizes/template/answer-viev.tpl.html",
  controllerAs: "$ctrl",
  bindToController: true,

  controller(AnswerService) {
    this.$onChanges = () => {
      if (this.selectedQuestion !== undefined) {
        this.refreshAnswer(this.selectedQuestion.id);
      }
    };

    this.selectAnswer = (draft) => {
      this.selectedAnswerTemp = { ...draft };
      this.selectedAnswer = { ...draft };
      this.modeQuestion = "";
    };

    this.addNewAnswer = () => {
      this.modeAnswer = "edit";
      this.selectedAnswerTemp = {
        id: "",
        id_question: this.selectedQuestion.id,
        answer: "",
        correct: false,
      };
    };
    this.editAnswer = (draft) => {
      this.selectedAnswerTemp = { ...draft };
      this.modeAnswer = "edit";
      this.modeQuestion = "";
    };
    this.cancelEditAnswer = () => {
      this.modeAnswer = "show";
    };
    this.deleteAnswer = () => {
      AnswerService.deleteAnswerById(this.selectedAnswerTemp.id);
      this.modeAnswer = "show";
      this.refreshAnswer(this.selectedQuestion.id);
    };
    this.saveAnswer = () => {
      if (this.selectedAnswerTemp.id === "") {
        this.selectedAnswerTemp.id =
        this.answers.length * 3 + Math.floor(Math.random() * 1000);
        AnswerService.addNewAnswer(this.selectedAnswerTemp);
        
      } else {
        AnswerService.updateAnswer(this.selectedAnswerTemp);

      }
      this.modeAnswer = "show";
      this.refreshAnswer(this.selectedQuestion.id);
    };
    this.showQuizList = () => {
      this.modeQuestion = "";
      this.modeAnswer = "";
      this.modeQuizList = "show";
    };
    this.showQuestionList = () => {
      this.modeQuestion = "show";
      this.modeAnswer = "";
      this.modeQuizList = "";
    };

    this.refreshAnswer = (id) => {
      AnswerService.fetchAnswers(id).then((data) => {
        this.answers = data;
        this.AnswersTemp = { ...this.Answers };
      });
    };
  },
});
