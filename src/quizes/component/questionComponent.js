quizes.component("quizQuestions", {
  bindings: {
    selectedQuiz: "<",
    modeQuestion: "=",
    onSelect: "&",
    modeQuizList: "=",
    selectedQuestion: "=",
    modeAnswer: "=",
  },
  templateUrl: "../src/quizes/template/question-viev.tpl.html",
  controllerAs: "$ctrl",
  bindToController: true,
  controller(QuestionsService) {
    this.$onInit = () => {};
    this.$onChanges = () => {
      if (this.selectedQuiz !== undefined)
        this.refreshQuestion(this.selectedQuiz.id);
    };

    this.selectQuestion = (draft) => {
      this.selectedQuestionTemp = { ...draft };
      this.selectedQuestion = { ...draft };
      this.modeQuizList = "";
      this.modeAnswer = "show";
    };

    this.addNewQuestion = () => {
      this.modeQuestion = "edit";
      this.modeAnswer = "";
      this.selectedQuestionTemp = {
        id: null,
        id_quiz: this.selectedQuiz.id,
        name: "",
      };
    };

    this.editQuestion = (draft) => {
      this.selectedQuestionTemp = { ...draft };
      console.log(this.selectedQuestionTemp)
      this.modeQuestion = "edit";
      this.modeQuizList = "";
    };
    this.cancelEditQuestion = () => {
      this.modeQuestion = "show";
    };
    this.deleteQuestion = () => {
      QuestionsService.deleteQuestionById(this.selectedQuestionTemp.id);
      this.modeQuestion = "show";
      this.refreshQuestion(this.selectedQuiz.id);
    };
    this.saveQuestion = () => {
      if (this.selectedQuestionTemp.id === "") {
        QuestionsService.updateQuestion(this.selectedQuestionTemp);
      } else {
        this.selectedQuestionTemp.id =
          this.questions.length * 3 + Math.floor(Math.random() * 1000);
        QuestionsService.addNewQuestion(this.selectedQuestionTemp);
      }
      this.modeQuestion = "show";
      this.refreshQuestion(this.selectedQuiz.id);
    };

    this.refreshQuestion = (id) => {
      QuestionsService.fetchQuestions(id).then((data) => {
        this.questions = data;
        this.questionsTemp = { ...this.questions };
      });
    };

    this.showQuizList = () => {
      this.modeQuestion = "";
      this.modeQuizList = "show";
      this.modeAnswer = "";
    };
  },
});
