angular.module("quizes").component("quizesList", {
  bindings: {
    modeQuizList: "=",
    quizes: "<",
    selectedQuiz: "=",
    onSelect: "&",
    onSave: "&",
    onDelete: "&",
    onCancel: "&",
  },
  templateUrl: "../src/quizes/template/quizesList-viev.tpl.html",
  controllerAs: "$ctrl",
  bindToController: true,
  controller($scope, QuizesService) {
    this.$onInit = () => {
      this.modeQuizList = "show";
    };
    this.edit = (quiz) => {
      this.modeQuizList = "edit";
      this.formMode = "edit";
      this.selectedQuiz = { ...quiz };
      this.selectedQuizTemp = { ...quiz };
    };

    this.creatNew = () => {
      this.modeQuizList = "edit";
      this.formMode = "new";
      this.selectedQuizTemp = { id: null, name: "", completed: false };
      this.changeAnswer = false;
    };
  },
});
