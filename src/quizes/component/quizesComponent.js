const quizes = angular.module("quizes", [
  "questions.service",
  "quizes.service",
  "answer.services",
]);
angular.module("quizes").component("quizesPage", {
  bindings: {},
  templateUrl: "../src/quizes/template/quizes-viev.tpl.html",
  controllerAs: "$ctrl",
  bindToController: true,
  controller($scope, QuizesService) {
    this.$onInit = () => {
      refresh();
      this.modeQuizList = "show";
      this.modeQuestion = "";
      this.modeAnswer = "";
    };
    refresh = () => {
      QuizesService.fetchQuizes().then((data) => {
        this.quizes = data;
      });
    };

    this.selectQuiz = (quiz) => {
      this.selectedQuiz = quiz;
      this.modeQuestion = "show";
    };
    this.saveQuiz = (draft) => {
      if (draft.id) {
        QuizesService.update(draft);
      } else {
        draft.id = this.quizes.length + 1;
        QuizesService.addNew(draft);
      }
      this.modeQuizList = "show";
      refresh();
    };
    this.deleteQuiz = (draft) => {
      QuizesService.deleteQuizById(draft.id);
      this.modeQuizList = "show";
      refresh();
    };
    this.cancelQuiz = () => {
      this.modeQuizList = "show";
    };
  },
});
